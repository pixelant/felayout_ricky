module.exports = {
    js: {
        src: '<%= dev %>/js/main.js',
        dest: '<%= tmp %>/main.js',
    },
    localJs: {
        src: '<%= dev %>/js/local.js',
        dest: '<%= tmp %>/local.js',
    }
};
