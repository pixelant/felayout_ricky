module.exports = {
    big: {
        src: '<%= big %>/partials/faviconsMetaTags.hbs',
        dest: '<%= big %>/partials/faviconsMetaTags.html'
    },
    small: {
        src: '<%= small %>/partials/faviconsMetaTags.hbs',
        dest: '<%= small %>/partials/faviconsMetaTags.html'
    }
};
