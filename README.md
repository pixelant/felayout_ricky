# [FElayout](https://bitbucket.org/pixelant/felayout_ricky) **0.3.3**
Frontend framework for **TYPO3**. Part of **Pixelant Core**.

[**Contributing**](#markdown-header-contributing)

[**Changelog**](#markdown-header-changelog)

***

###Before start work with FElayout, please read [documentation](https://bitbucket.org/pixelant/felayout_ricky/wiki)

Documentation:

* [Pixelant Core](https://docs.google.com/a/pixelant.se/document/d/1gA5JR7k3WfwqxTjFcQdZu-EPBPTxWAdPe6pBC1PMH80/edit?usp=sharing)
* [FElayout](https://bitbucket.org/pixelant/felayout_ricky/wiki)
* [t3layout](https://bitbucket.org/pixelant/t3layout_ricky/wiki/)
* [pxa\_generic_content](https://bitbucket.org/pixelant/pxa_generic_content)


### Required dependencies for this repository:

* Git
* [NodeJs](http://nodejs.org/)
    - **Node Version >=0.10.25**
    - **NPM Version >=1.4.14**
* [Bower](http://bower.io/) `npm install -g bower` **Version >=1.3.8**
* [Grunt](http://gruntjs.com/) `npm install -g grunt-cli`

***

#### Git instructions

[Git workflow](https://docs.google.com/a/pixelant.se/document/d/1gVPXDCBrR66Ahhh7c6BMBeQZ0KpWWxhlLEw6zNOZX1g/edit?usp=sharing)

[Semantic Versioning](http://semver.org/)

***

# Contributing

Everyone can add fixes to this repository. Here is several rules for contribute this repository. Please keep this in mind for better cooperation.

### Issues

If you have a question not covered in the documentation or want to report a bug, the best way to ensure it gets addressed is to file it in the appropriate issues tracker.

* Used the search feature to ensure that the bug hasn't been reported before
* Try to reduce your code to the bare minimum required to reproduce the issue. This makes it much easier (and much faster) to isolate and fix the issue.


### Fixes

If you want to fix bug by yourself or add new features, you have to use steps listed below.

* Add your fixes:
    1. Clone repository `git clone git@bitbucket.org:pixelant/felayout_ricky.git`
    2. Checkout to branch `dev` `git checkout -t origin/dev`
    3. Create new branch from `dev` `git checkout -b branchName`
    4. Add your changes, and commit it.
    5. Push your branch to repository `git push origin branchName`

* Recommend checking for issues, maybe somebody working with the same issue.
* Non-trivial changes should be discussed in an issue first.
* Lint the code by running `grunt check`.
* Dont forget about [editorconfig](http://editorconfig.org/).

***

***
#Changelog
***

# **0.3.3** (22.10.2015)
* Bug fixes

# **0.3.2** (18.09.2015)
* Added missing youtube icons
* Added new css variables to `customVariables.less`
* Bug fixes

# **0.3.1** (29.04.2015)
* Updated bootstrap to 3.3.4
* Bug fixes
* New favicons and way how to add it
* Added Print styles

# **0.3.0** (21.04.2015)
* update node js to v0.12.0 & npm v2.5.1 (http://theholmesoffice.com/node-js-fundamentals-how-to-upgrade-the-node-js-version/)
* Updated dependencies `package.json`
* Performance improvements
* Lot of css & js fixes
* New navigation menu `ExtendedMainNav`
* Updated search form styles

# **0.2.1** (25.03.2015)
* Added Bootstrap plugin to enable Bootstrap dropdowns to activate on hover
* Updated documentation

# **0.2.0** (25.02.2015)
* Minor bug fixes.
* Performance update.
* Updated dependencies
* Updated content elements. (Added Instagram icon to SocialIcons el.) 
* Parallax fixes
* Fixed icons in IconSelector for TYPO3

# **0.1.0** (20.10.2014)
* New files structure. Less partials, more content elements.
* New system how to build menu. Similar to TYPO3, added layout and hideInMenu variables.
* New 5-level menu
* New scalable header. Like we had footer in previos version of FElayout.
* New generic elements. Main menu, sub menu, breadcrumbs, language menu, menu toggle button, search form, search form toggle button.
* New templates for pages. Similar to templates on TYPO3.
* New config for icon fonts. Added selection.json and style.css from icomoon.

# **0.0.8** (22.08.2014)
* New Parallax
* Performance optimization
* Added force option to jshint and jscs code checker
* Update .gitignore

# **0.0.7** (08.08.2014)
* Added instructions for Designers
* IE fixes
* Fix Search form on startPage
* Stop news carousel
* new Parallax

# **0.0.6** (22.07.2014)
* Fixed css bugs
* Added documentation
* Update dependencies

# **0.0.5** (14.07.2014)

